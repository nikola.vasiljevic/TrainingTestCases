# TestCases for the DEXPI import and export of the software vendors. 


For abbreviations and Workflows of TestCase development, import and export see "DEXPI Test Cases DOC.*".

# Implementation status for the test cases

The interfaces of the SW tools have been tested by an external party (plants and bytes gbmh). After passing a minimum of certification points, a DEXPI certification is submitted to a software vendor. 

A list of actual certificates can be found here: https://dexpi.org/certificates/. 
